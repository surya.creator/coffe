$(document).ready(function(){
  $(".clickm").click(function(){
      $(".toggle").toggleClass("smt");
      $(".tglmenu").toggleClass("tgl");
  });
});

$("#button").click(function() {
  $('html, body').animate({
      scrollTop: $("#content-1").offset().top - 80
  }, 1000);
});

$("#button1").click(function() {
  $('html, body').animate({
      scrollTop: $("#content-1").offset().top - 80
  }, 1000);
});

$("#services").click(function() {
  $('html, body').animate({
      scrollTop: $("#content-2").offset().top - 80
  }, 1000);
});

$("#gallery").click(function() {
  $('html, body').animate({
      scrollTop: $("#content-3").offset().top - 80
  }, 1000);
});

$("#contact").click(function() {
  $('html, body').animate({
      scrollTop: $("#content-4").offset().top - 80
  }, 1000);
});

function validateContactUsForm(contact_us_form) {
  var name = contact_us_form["Name"];
  var email = contact_us_form["Email"];
  var message = contact_us_form["Message"];

  var error_name = document.getElementById("name_error");
  var error_email = document.getElementById("email_error");
  var error_message = document.getElementById("message_error");

  if (name.value == "") {
      setErrorFor(name, error_name, "Your name's empty");
      error_email.innerHTML = "";
      error_message.innerHTML = "";
      return false;
  }
  if(email.value == ""){
      setErrorFor(email, error_email, "Your email's empty");
      error_name.innerHTML = "";
      error_message.innerHTML = "";
      return false;
  }
  if(message.value == ""){
      setErrorFor(message, error_message, "Your message's empty");
      error_name.innerHTML = "";
      error_email.innerHTML = "";
      return false;
  }
}

function setErrorFor(field_selector, error_loc, error_message){
  field_selector.focus();
  field_selector.style.outlineWidth = "1px";
  field_selector.style.outlineColor = "#cc2828";
  error_loc.innerHTML = "<i class='fa fa-bell-o'></i><span>"+ error_message+"</span>";
}

$(document).ready(function () {

  var c, currentScrollTop = 0,
  navbar = $('#header');

  $(window).scroll(function () {
      var a = $(window).scrollTop();
      var b = navbar.height();
      if (a > 0) {
         navbar.addClass("bg-col");
      }else{
          navbar.removeClass("bg-col");
      }
  });
});
